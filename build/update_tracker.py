import hashlib
import re
import threading
import time

import requests
from bs4 import BeautifulSoup


def is_valid_url(ixcp):
    if re.search(
            r"^(?:(?:https?|ftp):)?//(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4])|(?:(?:[a-z0-9\u00a1-\uffff][a-z0-9\u00a1-\uffff_-]{0,62})?[a-z0-9\u00a1-\uffff]\.)+[a-z\u00a1-\uffff]{2,}\.?)(?::\d{2,5})?(?:[/?#]\S*)?$",
            ixcp):
        return True
    return False


class UpdateTracker(threading.Thread):
    def __init__(self, url, rep):
        super().__init__()
        self.url = url
        self.xurl: str = None
        self._exit = False
        self.rep = rep
    
    def run(self) -> None:
        while True:
            reg = re.search(r"https?://b(urning)?s(eries)?\.[a-z]{2,4}/", self.url, re.RegexFlag.IGNORECASE)
            sp = len(reg[0]) if reg else 0
            print("R")
            req = requests.get("https://jsonbase.com/bs-decaptcha/" + hashlib.md5(self.url[sp:].encode("ISO-8859-1")).hexdigest())
            if req.status_code == 200 and "broken" not in req.json() and len(BeautifulSoup(requests.get(req.json()["url"]).text, features="html.parser").find_all("video")) > 0:
                self.xurl = req.json()["url"]
                break
            time.sleep(1)
            if self._exit:
                break

    def exit(self):
        self._exit = True
