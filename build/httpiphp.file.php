<!DOCTYPE html>

<html>
<head>
<title>[{title}]</title>
<link href="data:image/x-icon;base64,AAABAAEAEBAAAAEAIABoBAAAFgAAACgAAAAQAAAAIAAAAAEAIAAAAAAAAAQAAAAAAAAAAAAAAAAAAAAAAAD/hAAZ/4QA//+EAP//hAAZAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA/4QAGf+EAP//hAD//4QA//+EABkAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA$">
</head>
<body>
<form action="./index.php" method="get">
  <label for="s">Season:</label>
  <select name="s" id="s" onchange="this.form.submit()">
  <option value="" selected="selected">Select Season</option>
[{options}]
  </select>
  <br /><br />
</form>
<?php
function zero($num){
    if($num < 10){
        return "0" . $num;
    } else {
        return $num;
    }
}

$db = json_decode(file_get_contents('dbi.json'), true);
if(isset($_GET['s']) && $_GET['s'] != -1){
    foreach ($db as $key=>$value){
        if($value["season"] == $_GET['s']){
            $season = $value['season'];
            $episode = $value['episode'];
            $title = $value['title'];
            echo("<a href=\"./$key\" target=\"_blank\">[Ep. " . zero($episode) . "]  " . $title . "</a><br />\n");
        }
    }
} else {
    foreach ($db as $key=>$value){
        $season = $value['season'];
        $episode = $value['episode'];
        $title = $value['title'];
        echo("<a href=\"./$key\" target=\"_blank\">[S" . $season . "E" . zero($episode) . "]  " . $title . "</a><br />\n");
    }
}
?>
</body>
</html>
