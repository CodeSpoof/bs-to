import base64
import hashlib
import os
import shutil
import json
import re
import time
import webbrowser
from os.path import isdir, isfile
from typing import Dict, Union

import dns.resolver
import qdarkgraystyle
import requests
import youtube_dl
from PyQt5 import QtWidgets, QtCore
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QFileDialog, QCompleter, QMessageBox
from os import mkdir

from bs4 import BeautifulSoup

from gui import Ui_MainWindow
from gui_popup import Ui_Dialog
from update_tracker import UpdateTracker
from urllib3.util import connection

broken_hosters = [
    "sendfox.org"
]
working_hosters = {
    "Vivo": ("/embed/", "/"),
    "VOE": ("/e/", "/"),
    "Vupload": ("/e/", "/"),
    "Vidoza": ("/embed-", "/"),
}
standard_hoster = "VOE"

# ------------------------
# DNS---------------------
_orig_create_connection = connection.create_connection


def myResolver(_host, dnssrv):
    r = dns.resolver.Resolver()
    r.nameservers = dnssrv
    answers = r.resolve(_host)
    for rdata in answers:
        return str(rdata)


def patched_create_connection(address, *args, **kwargs):
    """Wrap urllib3's create_connection to resolve the name elsewhere"""
    # resolve hostname to an ip address; use your own
    # resolver here, as otherwise the system resolver will be used.
    _host, _port = address
    _hostname = myResolver(_host, [
        "8.8.8.8",  # Google
        "8.8.4.4",  # Google
        "1.1.1.1",  # Cloudflare
        "1.0.0.1",  # Cloudflare
        "89.233.43.71",  # CensurfriDNS
        "130.225.244.166",  # CensurfriDNS
        "130.226.161.34",  # CensurfriDNS
        "185.38.27.139",  # CensurfriDNS
        "198.180.150.12",  # CensurfriDNS
        "37.235.1.177",  # FreeDNS
        "84.200.69.80",  # DNS.Watch
        "84.200.70.40",  # DNS.Watch
        "208.67.222.222",  # OpenDNS
        "208.67.220.220",  # OpenDNS
        "208.67.222.220",  # OpenDNS
        "208.67.220.222",  # OpenDNS
        "194.36.144.87",  # OpenNIC
        "172.104.237.57",  # FreeDNS
        "172.104.49.100",  # FreeDNS
        "45.33.97.5",  # FreeDNS
        "77.88.8.8",  # Yandex.DNS
        "77.88.8.1",  # Yandex.DNS
        "198.54.117.10",  # SafeServe
        "198.54.117.11",  # SafeServe
        "195.46.39.39",  # Safe DNS
        "195.46.39.40",  # Safe DNS
    ])

    return _orig_create_connection((_hostname, _port), *args, **kwargs)


connection.create_connection = patched_create_connection
# DNS---------------------
# ------------------------

lib_files = {}
env_files = {
    "httpiphp.file.php": "<!DOCTYPE html>\r\n\r\n<html>\r\n<head>\r\n<title>[{title}]</title>\r\n<link href=\"data:image/x-icon;base64,AAABAAEAEBAAAAEAIABoBAAAFgAAACgAAAAQAAAAIAAAAAEAIAAAAAAAAAQAAAAAAAAAAAAAAAAAAAAAAAD/hAAZ/4QA//+EAP//hAAZAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA/4QAGf+EAP//hAD//4QA//+EABkAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA$\">\r\n</head>\r\n<body>\r\n<form action=\"./index.php\" method=\"get\">\r\n  <label for=\"s\">Season:</label>\r\n  <select name=\"s\" id=\"s\" onchange=\"this.form.submit()\">\r\n  <option value=\"\" selected=\"selected\">Select Season</option>\r\n[{options}]\r\n  </select>\r\n  <br /><br />\r\n</form>\r\n<?php\r\nfunction zero($num){\r\n    if($num < 10){\r\n        return \"0\" . $num;\r\n    } else {\r\n        return $num;\r\n    }\r\n}\r\n\r\n$db = json_decode(file_get_contents(\'dbi.json\'), true);\r\nif(isset($_GET[\'s\']) && $_GET[\'s\'] != -1){\r\n    foreach ($db as $key=>$value){\r\n        if($value[\"season\"] == $_GET[\'s\']){\r\n            $season = $value[\'season\'];\r\n            $episode = $value[\'episode\'];\r\n            $title = $value[\'title\'];\r\n            echo(\"<a href=\\\"./$key\\\" target=\\\"_blank\\\">[Ep. \" . zero($episode) . \"]  \" . $title . \"</a><br />\\n\");\r\n        }\r\n    }\r\n} else {\r\n    foreach ($db as $key=>$value){\r\n        $season = $value[\'season\'];\r\n        $episode = $value[\'episode\'];\r\n        $title = $value[\'title\'];\r\n        echo(\"<a href=\\\"./$key\\\" target=\\\"_blank\\\">[S\" . $season . \"E\" . zero($episode) . \"]  \" . $title . \"</a><br />\\n\");\r\n    }\r\n}\r\n?>\r\n</body>\r\n</html>"
}


class g:
    stopLoad = False
    isLoading = False
    envPath = None
    updatedText = ""
    loader = None
    lid = {}
    d = {}
    max = 1
    val = 1


gl = g()


class InvalidURLException(Exception):
    pass


def is_valid_url(ixcp):
    if re.search(
            r"^(?:(?:https?|ftp):)?//(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4])|(?:(?:[a-z0-9\u00a1-\uffff][a-z0-9\u00a1-\uffff_-]{0,62})?[a-z0-9\u00a1-\uffff]\.)+[a-z\u00a1-\uffff]{2,}\.?)(?::\d{2,5})?(?:[/?#]\S*)?$",
            ixcp):
        return True
    return False


def finishedHandler():
    ui.buttonApplySearch.setEnabled(True)
    updateManagerTable()


def bs_url_dict(u: str):
    regex = r"(?P<host>https?://(?:bs\.to|burningseries\.(?:co|sx|ac|vc|cx|ma|nz))/)(?:serie/)(?P<show>[^/]+)(?:/(?P<season>[0-9]+)(?:/(?P<episode>[^/]+)(?:/(?P<lang>[a-z]+)(?:/(?P<hoster>[^/]+))?)?)?)?(?:/)?"
    matches = re.search(regex, u)
    return matches.groupdict()


def lcs(str1: str, str2: str) -> str:
    for i in range(1, len(str1)):
        if not str2.__contains__(str1[0:i]):
            return str1[0:i - 1]
    return str1


def progress():
    ui.progressGeneral.setMaximum(gl.max)
    ui.progressGeneral.setValue(gl.val)


def vd(x, title, folder):
    ydl_opts = {'outtmpl': os.path.join(gl.envPath, "videos", folder, title), 'no_warnings': True, "quiet": True}
    with youtube_dl.YoutubeDL(ydl_opts) as ydl:
        ydl.download([x])


def get_series(url: str) -> Dict[str, Union[str, dict]]:
    url_parts = bs_url_dict(url)
    if url_parts:
        html = BeautifulSoup(requests.get(host + url_parts['show'], headers={"Accept-Language": "de-DE,de;q=0.5"}).text,
                             features="html.parser")
        ret = {"title": lcs(html.find("title").text, html.find("div", {"id": "sp_left"}).find("h2").text),
               "url": host + url_parts['show']}
        cover = requests.get(url_parts['host'] + html.find("div", {"id": "sp_right"}).find("img")["src"],
                             headers={"Accept-Language": "de-DE,de;q=0.5"})
        ret["cover"] = f"data:{cover.headers['content-type']};base64,{str(base64.b64encode(cover.content), 'utf-8')}"
        ret["desc"] = html.find("div", {"id": "sp_left"}).find("p", recursive=False).text
        ret["videos"] = {}
        print("Calling base page...")
        print("Extracting seasons...")
        for block in html.find("div", {"id": "seasons"}).find_all("a"):
            if len(block.text) < 2:
                ret["videos"]["Season " + block.text] = {"url": url_parts['host'] + block["href"]}
            else:
                ret["videos"][block.text] = {"url": url_parts['host'] + block["href"]}
        iterax = 1
        for season in ret["videos"]:
            print(f"Fetching episodes of season {iterax}/{len(ret['videos'])}...")
            iterax += 1
            html = BeautifulSoup(
                requests.get(ret["videos"][season]["url"], headers={"Accept-Language": "de-DE,de;q=0.5"}).text,
                features="html.parser")
            episodes = html.find("table", {"class": "episodes"}).find_all("tr")
            ret["videos"][season]["episodes"] = {}
            for episode in episodes:
                tag = episode.find_all("td")[0].find("a")
                ret["videos"][season]["episodes"][tag.text] = {"title": tag["title"],
                                                               "url": url_parts['host'] + f"{tag['href']}/",
                                                               "episode": tag.text, "season":
                                                                   ret["videos"][season]["url"].replace(
                                                                       host + url_parts['show'],
                                                                       "").split("/")[
                                                                       1]}
        iterax = 1
        for season in ret["videos"]:
            print(f"Fetching episode descriptions of season {iterax}/{len(ret['videos'])}...")
            iterax += 1
            for episode in ret["videos"][season]["episodes"]:
                ret["videos"][season]["episodes"][episode]["desc"] = BeautifulSoup(
                    requests.get(ret["videos"][season]["episodes"][episode]["url"],
                                 headers={"Accept-Language": "de-DE,de;q=0.5"}).text,
                    features="html.parser").find(
                    "p", {"id": "spoiler"}).text
    else:
        QMessageBox.question(application, 'Error', "URL not supported", QMessageBox.Ok)
        raise InvalidURLException("Not a valid burningseries URL")
    return ret


class Loader(QtCore.QThread):
    statusUpdateDisplay = QtCore.pyqtSignal()
    statusFinished = QtCore.pyqtSignal()
    statusUpdateHook = QtCore.pyqtSignal()
    url = ""

    def displayUpdate(self, text: str):
        gl.updatedText = text
        self.statusUpdateDisplay.emit()

    def run(self):
        try:
            webbrowser.get('firefox')
        except webbrowser.Error:
            QMessageBox.question(application, 'Firefox missing',
                                 "To use this feature,\n please install Firefox.",
                                 QMessageBox.Ok)
            self.terminate()
        self.displayUpdate("Loading Projects...")
        if not isdir(os.path.join(gl.envPath, "bs2")):
            mkdir(os.path.join(gl.envPath, "bs2"))
        projects = {}
        if isfile(os.path.join(gl.envPath, "bs2", "projects.json")):
            projects = json.load(open(os.path.join(gl.envPath, "bs2", "projects.json"), "r"))
        self.displayUpdate("Loading Series...")
        inp = self.url
        db = {}
        if inp in projects:
            db = json.load(open(os.path.join(gl.envPath, "bs2", f"{inp}.json"), "r"))
        else:
            db = get_series(inp)
            if isfile(os.path.join(gl.envPath, "bs2", f"{db['url']}.json")):
                db = json.load(open(os.path.join(gl.envPath, "bs2", f"{inp}.json"), "r"))
            else:
                projects[bs_url_dict(inp)["show"]] = host + bs_url_dict(inp)["show"]
                json.dump(projects, open(os.path.join(gl.envPath, "bs2", "projects.json"), "w+"))
        url_parts = bs_url_dict(db["url"])
        self.displayUpdate("Initiating Episode Checkout...")
        gl.max = 0
        for season in db["videos"]:
            for _ in db["videos"][season]["episodes"]:
                gl.max += 1
        gl.val = 1
        try:
            for season in db["videos"]:
                for episode in db["videos"][season]["episodes"]:
                    if "xurl" not in db["videos"][season]["episodes"][episode] or re.search(r"(?<=://)(?:[a-z]+?\.)+[a-z]{2,}", db["videos"][season]["episodes"][episode]["xurl"])[0] in broken_hosters:
                        self.displayUpdate("Loading Episode Page...")
                        print("Retrieving Episode " + db["videos"][season]["episodes"][episode]["title"])
                        reg = re.search(r"https?://b(?:urning)?s(?:eries)?\.[a-z]{2,4}/", db["videos"][season]["episodes"][episode]["url"], re.RegexFlag.IGNORECASE)
                        sp = len(reg[0]) if reg else 0
                        found = False
                        for hoster in working_hosters:
                            req = requests.get("https://jsonbase.com/bs-decaptcha/" + hashlib.md5(str(db["videos"][season]["episodes"][episode]["url"][sp:] + hoster).encode("ISO-8859-1")).hexdigest())
                            if (req.status_code == 200 and not "broken" in req.json()) and len(BeautifulSoup(requests.get(req.json()["url"]).text, features="html.parser").find_all("video")) > 0:
                                inp = req.json()["url"].replace(working_hosters[hoster][0], working_hosters[hoster][1])
                                found = True
                                break
                        if not found:
                            webbrowser.get('firefox').open_new_tab(db["videos"][season]["episodes"][episode]["url"] + standard_hoster)
                            self.displayUpdate("Waiting for Update...")
                            observers = []
                            for hoster in working_hosters:
                                observers.append(UpdateTracker(db["videos"][season]["episodes"][episode]["url"] + hoster, working_hosters[hoster]))
                                observers[len(observers) - 1].start()
                            while not any(x.xurl is not None for x in observers):
                                time.sleep(0.1)
                            inp = next((x.xurl.replace(x.rep[0], x.rep[1]) for x in observers if x.xurl is not None), None)
                            _ = (x.exit() for x in observers)
                            print("Found")
                        db["videos"][season]["episodes"][episode]["xurl"] = inp
                        self.displayUpdate("Saving details...")
                        json.dump(db, open(os.path.join(gl.envPath, "bs2", f"{url_parts['show']}.json"), "w+"), indent=4)
                        print("Found")
                    else:
                        print("Skipped")
                    gl.val += 1
                    progress()
        except KeyboardInterrupt:
            json.dump(db, open(os.path.join(gl.envPath, "bs2", f"{url_parts['show']}.json"), "w+"), indent=4)
            exit(0)
        self.displayUpdate("Initiating Downloader...")
        try:
            mkdir(os.path.join(gl.envPath, "videos", url_parts['show']))
        except Exception:
            pass
        dbi = {}
        gl.val = 1
        progress()
        for season in db["videos"]:
            for episode in db["videos"][season]["episodes"]:
                try:
                    print(db["videos"][season]["episodes"][episode]["xurl"])
                    print(
                        f'S{db["videos"][season]["episodes"][episode]["season"]}E{db["videos"][season]["episodes"][episode]["episode"]}.mp4')
                    self.displayUpdate(f'Downloading {db["videos"][season]["episodes"][episode]["xurl"]}...')
                    vd(db["videos"][season]["episodes"][episode]["xurl"],
                       f'S{db["videos"][season]["episodes"][episode]["season"]}E{db["videos"][season]["episodes"][episode]["episode"]}.mp4',
                       url_parts['show'])
                    self.displayUpdate("Saving details...")
                    dbi[
                        f'S{db["videos"][season]["episodes"][episode]["season"]}E{db["videos"][season]["episodes"][episode]["episode"]}.mp4'] = {
                        "season": db["videos"][season]["episodes"][episode]["season"],
                        "episode": db["videos"][season]["episodes"][episode]["episode"],
                        "title": db["videos"][season]["episodes"][episode]["title"]}
                    gl.val += 1
                    progress()
                except KeyboardInterrupt:
                    exit(1)
                except Exception:
                    pass
        self.displayUpdate("Creating Webpage (PHP)...")
        json.dump(dbi, open(os.path.join(gl.envPath, "videos", url_parts['show'], "dbi.json"), "w+"))
        index_php = open(os.path.join(gl.envPath, "httpiphp.file.php")).read()
        index_php = index_php.replace("[{title}]", db["title"])
        pz = ""
        for season in db["videos"]:
            pz = pz + f"    <option value=\"{db['videos'][season]['episodes']['1']['season']}\">{season}</option>\n"
        index_php = index_php.replace("[{options}]", pz)
        open(os.path.join(gl.envPath, "videos", url_parts['show'], "index.php"), "w+").write(index_php)
        self.statusFinished.emit()


class ApplicationWindow(QtWidgets.QMainWindow):
    def __init__(self):
        super(ApplicationWindow, self).__init__()

        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)


def zeros(num):
    if int(num) >= 10:
        return str(num)
    else:
        return "0" + str(num)


def legal(fn: str):
    remove = [("\"", ""), ("'", "'"), ("`", "'"), ("/", "|"), ("\\", "|"), ("^", ""), ("~", ""), ("?", "")]
    for x, y in remove:
        fn = fn.replace(x, y)
    return fn


def updateDisplay():
    ui.statusBar.showMessage(gl.updatedText, 5000)


def toggleLoad(url):
    if gl.isLoading:
        gl.stopLoad = True
        gl.loader.requestInterruption()
    else:
        ui.buttonApplySearch.setDisabled(True)
        gl.stopLoad = False
        gl.loader = Loader()
        gl.loader.url = url
        gl.loader.statusUpdateDisplay.connect(updateDisplay)
        gl.loader.statusFinished.connect(finishedHandler)
        gl.loader.statusUpdateHook.connect(progress)
        gl.loader.start()
    updateManagerTable()


def browseOpenEnv():
    ui.textEnvironmentOpenPath.setText(str(QFileDialog.getExistingDirectory(ui.centralwidget, "Select Safe-Directory")))


def openEnv():
    for file in env_files:
        open(os.path.join(ui.textEnvironmentOpenPath.text(), file), "w+").write(env_files[file])
    if isfile(os.path.join(ui.textEnvironmentOpenPath.text(), "httpiphp.file.php")):
        gl.envPath = ui.textEnvironmentOpenPath.text()
        ui.tabWidget.setTabEnabled(0, False)
        ui.tabWidget.setTabEnabled(1, True)
        ui.tabWidget.setTabEnabled(2, True)
        updateManagerTable()


def updateManagerTable():
    projects = {}
    if isfile(os.path.join(gl.envPath, "bs2", "projects.json")):
        projects = json.load(open(os.path.join(gl.envPath, "bs2", "projects.json"), "r"))
    d = []
    for iden in projects:
        se = []
        ser = json.load(open(os.path.join(gl.envPath, "bs2", f"{iden}.json")))
        se.append(ser["title"])
        se.append(sum([len(ser["videos"][x]["episodes"]) for x in ser["videos"]]) == sum(
            [sum([("xurl" in ser["videos"][y]["episodes"][x]) for x in ser["videos"][y]["episodes"]]) for y in
             ser["videos"]]))
        se.append(str(len(ser["videos"]) - ("Specials" in ser["videos"])) + (" + Specials" if "Specials" in ser["videos"] else ""))
        se.append(str(sum([len(ser["videos"][x]["episodes"]) for x in ser["videos"]])))
        se.append(str(isdir(os.path.join(gl.envPath, "readable", "videos", iden))))
        se.append(ser["url"])
        se.append(iden)
        d.append(se)
    ui.populate(d)


def tableDoubleClicked():
    if len(ui.tableManager.selectedIndexes()) == 1:
        _id = str(
            ui.tableManager.model().data(ui.tableManager.model().index(ui.tableManager.selectedIndexes()[0].row(), 6)))
        co = ui.tableManager.selectedIndexes()[0].column()
        if co == 0:
            dialog = Ui_Dialog(json.load(open(os.path.join(gl.envPath, "bs2", f"{_id}.json"), "r")))
            dialog.setStyleSheet(qdarkgraystyle.load_stylesheet())
            dialog.exec_()
        elif co == 1:
            if not gl.loader.isRunning() and not QMessageBox.question(application, 'Restart',
                                                                      "Do you really want to restart?",
                                                                      QMessageBox.Yes | QMessageBox.No,
                                                                      QMessageBox.No) == QMessageBox.No:
                try:
                    shutil.rmtree(os.path.join(gl.envPath, "videos", _id))
                    shutil.rmtree(os.path.join(gl.envPath, "readable", "videos", _id))
                except Exception:
                    pass
                url = json.load(open(os.path.join(gl.envPath, "bs2", f"{_id}.json"), "r"))["url"]
                os.remove(os.path.join(gl.envPath, "bs2", f"{_id}.json"))
                gl.stopLoad = False
                ui.buttonApplySearch.setDisabled(True)
                gl.loader = Loader()
                gl.loader.url = url
                gl.loader.statusUpdateDisplay.connect(updateDisplay)
                gl.loader.statusFinished.connect(finishedHandler)
                gl.loader.statusUpdateHook.connect(progress)
                gl.loader.start()
        elif co == 3:
            if QMessageBox.question(application, 'Delete', "Do you want to delete this series?",
                                    QMessageBox.Yes | QMessageBox.No, QMessageBox.No) == QMessageBox.No:
                return
            try:
                shutil.rmtree(os.path.join(gl.envPath, "readable", "videos", _id))
            except Exception:
                pass
            try:
                shutil.rmtree(os.path.join(gl.envPath, "videos", _id))
            except Exception:
                pass
            try:
                shutil.rmtree(os.path.join(gl.envPath, "bs2", f"{_id}.json"))
            except Exception:
                pass
            projects = json.load(open(os.path.join(gl.envPath, "bs2", "projects.json")))
            del projects[_id]
            json.dump(projects, open(os.path.join(gl.envPath, "bs2", "projects.json"), "w+"))
            updateManagerTable()
        elif co == 4 and (sys.platform.startswith("linux") or sys.platform.startswith("win")):
            if str(ui.tableManager.model().data(
                    ui.tableManager.model().index(ui.tableManager.selectedIndexes()[0].row(), 1))) == str(True):
                folder = os.path.join("videos", _id)
                if not isdir(os.path.join(gl.envPath, "readable", folder)):
                    x = json.load(open(os.path.join(gl.envPath, folder, "dbi.json"), "r"))
                    try:
                        mkdir(os.path.join(gl.envPath, "readable"))
                    except Exception:
                        pass
                    try:
                        mkdir(os.path.join(gl.envPath, "readable", "videos"))
                    except Exception:
                        pass
                    try:
                        mkdir(os.path.join(gl.envPath, "readable", folder))
                    except KeyboardInterrupt:
                        pass
                    for file in x:
                        fxs = f'[S{x[file]["season"]}E{zeros(x[file]["episode"])}]  {legal(x[file]["title"])}.mp4'
                        os.link(f"{os.path.join(gl.envPath, folder, file)}",
                                f"{os.path.join(gl.envPath, 'readable', folder, fxs)}")
                else:
                    shutil.rmtree(os.path.join(gl.envPath, "readable", folder))
                updateManagerTable()
            else:
                QMessageBox.question(application, 'Readable Module', "Only for finished shows!", QMessageBox.Ok)
        elif co == 5:
            if QMessageBox.question(application, 'Repair', "Do you want to redownload this series?",
                                    QMessageBox.Yes | QMessageBox.No, QMessageBox.No) == QMessageBox.No:
                return
            if not gl.loader.isRunning():
                shutil.rmtree(os.path.join(gl.envPath, "videos", _id))
                gl.stopLoad = False
                ui.buttonApplySearch.setDisabled(True)
                gl.loader = Loader()
                gl.loader.url = _id
                gl.loader.statusUpdateDisplay.connect(updateDisplay)
                gl.loader.statusFinished.connect(finishedHandler)
                gl.loader.statusUpdateHook.connect(progress)
                gl.loader.start()


def applyClicked():
    try:
        projects = {}
        if isfile(os.path.join(gl.envPath, "bs2", "projects.json")):
            projects = json.load(open(os.path.join(gl.envPath, "bs2", "projects.json"), "r"))
        if gl.lid[ui.textSearch.text()] in projects:
            toggleLoad(gl.lid[ui.textSearch.text()])
        else:
            toggleLoad(host + gl.lid[ui.textSearch.text()])
    except Exception:
        QMessageBox.question(application, 'Search', "Series not found.", QMessageBox.Ok)


if __name__ == "__main__":
    import sys

    gl.loader = Loader()
    app = QtWidgets.QApplication(sys.argv)
    application = ApplicationWindow()
    application.setStyleSheet(qdarkgraystyle.load_stylesheet())
    ui = application.ui
    gl.loader.statusUpdateDisplay.connect(updateDisplay)
    gl.loader.statusFinished.connect(finishedHandler)
    gl.loader.statusUpdateHook.connect(progress)
    ui.buttonEnvironmentOpenBrowse.clicked.connect(browseOpenEnv)
    ui.buttonEnvironmentOpenOpen.clicked.connect(openEnv)
    ui.buttonApplySearch.clicked.connect(applyClicked)
    ui.tableManager.doubleClicked.connect(tableDoubleClicked)
    ui.tabWidget.setTabEnabled(1, False)
    ui.tabWidget.setTabEnabled(2, False)
    try:
        htm = BeautifulSoup(requests.get("https://bs.to/andere-serien").text, features="html.parser")
        host = "https://bs.to/serie/"
    except requests.exceptions.ConnectionError:
        htm = BeautifulSoup(requests.get("https://burningseries.vc/andere-serien").text, features="html.parser")
        host = "https://burningseries.vc/serie/"
    li = []
    gl.lid = {}
    for genre in htm.find_all("div", {"class": "genre"}):
        gen = genre.find("strong").text
        for serie in genre.find_all("li"):
            li.append(f"{serie.find('a')['title']} ({gen})")
            gl.lid[f"{serie.find('a')['title']} ({gen})"] = serie.find('a')['href'].split('/')[-1]
    compl = QCompleter(li)
    compl.setCaseSensitivity(Qt.CaseInsensitive)
    compl.setModelSorting(QCompleter.CaseInsensitivelySortedModel)
    compl.setFilterMode(Qt.MatchContains)
    ui.textSearch.setCompleter(compl)
    application.show()
    sys.exit(app.exec_())
