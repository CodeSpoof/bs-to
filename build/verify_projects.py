import json
import os
import requests
from bs4 import BeautifulSoup

projects = json.load(open(os.path.join(input("Path: "), "bs2", "projects.json"), "r"))
for _id in projects:
    try:
        req = BeautifulSoup(requests.get(projects[_id]).text, features="html.parser")
        if len(req.find_all("section", attrs={"class": "andere-serien"})) > 0:
            print(projects[_id] + " ==> Error")
        else:
            print(projects[_id] + " ==> OK")
    except:
        print(projects[_id] + " ==> Error")
