# Introduction
This project aims to help users download whole shows from Burningseries (<a href="https://bs.to/">bs.to</a>). Sadly BS has protected every single video with a reCAPTCHA. So this tool aims to help you as far as possible
# Structure
This tool is just one part. It is a python program, that manages the file part. Because BS detects webdrivers such as selenium (chromedriver, geckodriver, etc.) and I don't want to do things like mitm the browser to remove such things, I made a Firefox Plugin, that automatically copies links as soon as you solved the CAPTCHA. That means, this program is the manager that opens the links in the browser, while the plugin uses the clipboard as simple method of data transfer.
# Build
- run <code>python3 -m pip install -f build/requirements.txt --target build</code>
- cd into build directory
- run <code>python3 -m zipapp -o bs2ui.pyz -p /usr/bin/python3 -c build</code>
# Installation
Very simple. Just run the .pyz file using python 3.5 or above. It is also strongly recommended using the Plugin installation feature before downloading. As long as you have installed a recent version of Firefox, there should be no issues running the program.
# Usage
This software works on the base of environments.
That means:
  - You can have seperated folders e.g., several users
  - Portability: Can be used from a USB-Stick
  - Privacy: No files are written outside the environment directories
  - Standalone: Your environment folder is working "as is"

When you start the program, it won't let you switch to any tabs, you're basically stuck in the Environment tab, because the program needs you to create or open an environment first.

Creating is as simple as choosing a directory to create it. Don't worry, if you create a new environment in the folder of an existing one, you won't lose data, as long as don't execute any further actions.

Opening is just as simple: Just select the folder, you selected when creating the environment.

After you created or opened an environment, it is basically ready for use, which leads directly to the next topic:
## Features+Tutorials
- Download from every domain of BS
    - (Search for the show in the search field, select the show from the completion-list and click "Apply")
      
      or
  
    - (Paste the URL to a show into the URL-textfield in the Downloader-Tab. Doesn't matter if it is the show or one of its episodes, the whole show will be downloaded anyway.)
    - Press Load to start the downloading process. This will open tabs in Firefox (one for each episode).
        
        !!BE AWARE, THAT WHILE THE DOWNLOAD IS RUNNING YOU CAN'T COPY ANYTHING, OR IT WILL BREAK THE DOWNLOAD!!
    
    - When the downloader is running, it will open a new tab, as soon as it detects a newly copied URL in the clipboard. That means, you'll either have to have the plugin installed or you must copy the links, that will be loaded/copy the source of the loaded iframe.<br />
      (Plugin strongly recommended)<br />
      For the link/iframe to appear, a CAPTCHA needs to be solved.<br />
      If you copied the URL, the next tab will be opened within a few seconds. With the Plugin, this will happen automatically.<br />
      !!ATTENTION: THE ONLY SUPPORTED HOSTS ARE VOE, VIDOZA AND VUPLOAD!!<br />
      When you encounter an episode, that isn't available on one of these hosts, you can also copy urls from other websites to hosts supported by youtube-dl. You also can skip downloading this episode at your own risk, by copying just any url.<br />
      The progress on the total episodes is displayed on the interface.
    - When every CAPTCHA is solved, the program will start downloading the videos automatically. The progress is displayed.<br />
      You can pause and continue solving CAPTCHAs later by just closing the interface. Progress is automatically saved.
    - Wait for the downloading to finish off, then the table in the Manager-Tab will be updated.
- Continue solving CAPTCHAS for partially finished shows
    - !!Search for the show!!
    - Select the show from the completion-list
    - Click "Apply" (The show-ID will be set into the URL textfield)
    - Click "Load" to continue solving CAPTCHAs
- Display infos about a show
    - Doubleclick the title of a show in the table in the Manager-Tab
- Restart solving CAPTCHAs (for new episodes?)
    - Doubleclick in the "finished" column to restart collecting urls for that show
- Delete shows
    - Doubleclick in the "videos" column to delete the whole show from your device
- Web-Interface to Humanly readable format
    - On default shows are saved to be served to the browser using a webserver like Apache together with PHP. If you want to have the episodes in a format that can be understood easier by humans, doubleclick in the "readable" column to toggle the human-readability. This won't take up any more disk space, even if otherwise displayed.
- Redownload shows (In cases of failed downloads)
    - First try starting as if to continue downloading the show
    - If the first method didn't help, doubleclick in the "URL" column. This won't need you to solve any CAPTCHAs as URLs are cached.

#Plugin
As the extensions for Firefox are just renamed zips, you can simply open the xpi file with an archive manager. As that project is completely finished and won't be updated, there won't be a separate git for that. The only thing unreadable in there are the signatures by Mozilla, so you can inspect any part of the actual code.
