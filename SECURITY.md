# Security Policy

## Supported Versions

Security updates (when some vulnerabilities occure) will always be published for the newest version and two versions back

## Reporting a Vulnerability

Vulnerabilities should be reported to *baum.looping@gmail.com*
Please also create an issue for that vulnerability, but information, that can be used to reproduce the vulnerability, should only be in the email (mention the mail topic in the issue)
